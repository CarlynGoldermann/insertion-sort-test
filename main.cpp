/************************************************************************
Author:         Randall D. Rowland Jr.
Class:          CS216
Instructor:     Dan Randall
Date:           26 Jan 2014
Description:    Will take an array of Items and sort it using an insertion
                sort. Will record the time of how long it took the sort to
                complete. Test the insertion sort on large sets of random
                data, sorted, and reverse-sorted data, and record the performance
                results.

Compilation instructions:       Requires insertsort.h and duration.h
Known bugs/missing features:    None

Modifications:
 Date                Comment
-------    ------------------------------------------------

************************************************************************/
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include "duration.h"
#include "insertsort.h"

using namespace std;

int main()
{
    //Setup variables
    const int SIZE = 100000;
    clock_t start, finish;
    int array[SIZE];

    //seed rand()
    srand(time(NULL));

    cout << "Filling array with random data: ";
    //fill the array with random int
    for(int i = 0; i < SIZE; i++)
    {
        array[i] = rand() % SIZE;
    };
    for(int i = 0; i < 17; i++)     //print the first few numbers stored in the array
    {
        cout << " " << array[i] << ",";
    }
    cout << "..." << endl << endl;


    cout << "Begin insertion sort on random data..." << endl;

    start = clock();                //start the clock
    insertion(array, 0, SIZE - 1);  //sort the array
    finish = clock();               //stop the clock

    cout << "Random data has been sorted in the array: ";
    for(int i = 0; i < 17; i++)     //print the first few numbers in the stored array
    {
        cout << " " << array[i] << ",";
    }
    cout << "..." << endl << endl;


    cout << "It took the sort " << duration(start, finish) << "seconds to sort random data!" << endl << endl;

    //Array now has been sorted, let's run the sort function again on sorted data!
    cout << "Run insertion sort on the already sorted array!" << endl;

    start = clock();                //start the clock
    insertion(array, 0, SIZE - 1);  //sort the array
    finish = clock();               //stop the clock

    cout << "It took the sort " << duration(start, finish) << "seconds to sort sorted data!" << endl << endl;

    //REVERSE SORT
    int temp = SIZE;
    for(int i = 0; i < SIZE; i++)   //Fill the array with a count donw of numbers
    {
        array[i] = temp--;
    }
    cout << "Reverse sorted data has been put in the array: ";
    for(int i = 0; i < 17; i++)
    {
        cout << " " << array[i] << ",";
    }
    cout << "..." << endl << endl;

    cout << "Run insertion sort on reverse sorted data!" << endl;
    start = clock();                //start the clock
    insertion(array, 0, SIZE - 1);   //sort the array
    finish = clock();               //stop the clock

    cout << "After sort: ";
    for(int i = 0; i < 17; i++)
    {
        cout << " " << array[i] << ",";
    }
    cout << "..." << endl << endl;

    cout << "It took the sort " << duration(start, finish) << "seconds to sort reverse sorted data!" << endl << endl;

    return 0;
}
