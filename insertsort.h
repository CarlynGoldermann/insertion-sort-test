/************************************************************************
Author:         Randall D. Rowland Jr.
Class:          CS216
Instructor:     Dan Randall
Date:           24 Jan 2014
Description:    Header file containing a function that will perform a
                insertion sort on an array. Insertion sort function was
                not written by me. I only added comments/whitespace to
                make it more readable. Used MLA format to cite the book
                I retrieved the function from since I do not know who
                actually wrote the function. License for function is unknown.

Compilation instructions:       Standard
Known bugs/missing features:    None

Modifications:
 Date                   Comment
-------     ------------------------------------------------
26JAN2014   Added exch() and compexch() which is required for the sort
            Moved exch() and compexch() to their own file for reuse with more sorts
            Include file that contains exch() and compexch()
************************************************************************/
#ifndef INSERTSORT_H_INCLUDED
#define INSERTSORT_H_INCLUDED

#include "exchange.h"       //required for the sort to function

/************************************************************************
    Function:       insertion(Item, int, int)
    Author:         Sedgewick, Robert. Algorithms in C++. Westford: Addison-Wesley
                        Publishing Company, 1998. Print.
    Description:    Insertion sort is a stable simple implementation of a comparison
                    sort algorithm. It is much less effiecient on large lists.
    Parameters:
        a[]         An array of items that is to be sorted
        l           The left side of the array or starting point
        r           The right side of the array or ending point
    Returns:

************************************************************************/
template <class Item>
void insertion(Item a[], int l, int r)
{
    int i;
    for(i = r; i > l; i--)
    {
        compexch(a[i - 1], a[i]);
    }
    for(i = l + 2; i <= r; i++)
    {
        int j = i;
        Item v = a[i];
        while(v < a[j - 1])
        {
            a[j] = a[j - 1];
            j--;
        }
        a[j] = v;
    }
}

#endif // INSERTSORT_H_INCLUDED
