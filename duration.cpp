/************************************************************************
Author:     Randall D. Rowland Jr.
Class:      CS216
Instructor: Dan Randall
Date:       26 Jan 2014

Description:

Compilation instructions:       Standard
Known bugs/missing features:    None
Modifications:
 Date                   Comment
-------     ------------------------------------------------
26JAN2013   removed the double variable and return the formula
************************************************************************/
#include "duration.h"

 /************************************************************************
    Function: duration(const clock_t, const clock_t)
    Author: Randall D. Rowland Jr.
    Description:    Takes two clock_t parameters and finds the time difference
                    between the two and returns it in seconds.
    Parameters:
        s   Should be the starting time
        f   Should be the finishing time
    Returns:
        This will be the time difference between the two parameters in seconds
 ************************************************************************/
double duration(const clock_t s, const clock_t f)
{
    return (double)(f - s) / CLOCKS_PER_SEC;
};
