/************************************************************************
Author:         Randall D. Rowland Jr.
Class:          CS216
Instructor:     Dan Randall
Date:           26 Jan 2014
Description:

Compilation instructions:       Standard
Known bugs/missing features:    None

Modifications:
 Date                   Comment
-------     ------------------------------------------------

************************************************************************/
#ifndef EXCHANGE_H_INCLUDED
#define EXCHANGE_H_INCLUDED

/************************************************************************
    Function:       exch(Item&, Item&)
    Author:         Sedgewick, Robert. Algorithms in C++. Westford: Addison-Wesley
                        Publishing Company, 1998. Print.
    Description:    Has two Items switch places
    Parameters:
        A           The first Item to be placed in the second Item's spot
        B           The second Item to be place in the first Item's spot
    Returns:

************************************************************************/
template <class Item>
void exch(Item &A, Item &B)
{
    Item temp = A;
    A = B;
    B = temp;
}

/************************************************************************
    Function:       compexch(Item&, Item&)
    Author:         Sedgewick, Robert. Algorithms in C++. Westford: Addison-Wesley
                        Publishing Company, 1998. Print.
    Description:    Compares two Items and if the second Item is less than
                    the first Item it calls exch().
    Parameters:
        A           The first Item to be compared
        B           The second Item to be compared
    Returns:

************************************************************************/
template <class Item>
void compexch(Item &A, Item &B)
{
    if(B < A)
    {
        exch(A, B);
    }
}


#endif // EXCHANGE_H_INCLUDED
